import getDB
import get_data_from_db
import pandas as pd

STATUS = ''
FLAGGER_BUY = 0
FLAGGER_SELL = 0


def start(companies, n_rsi, n_dd):
    for company in companies:
        double_divergency(get_data_from_db.get_historical_data_from_table('data', company), n_rsi, company, n_dd)


def double_divergency(data, n_rsi, company, n_dd):
    global STATUS
    print('dd for: ', company)
    date = list(data['date'][n_rsi:])
    price_high = list(data['high'][n_rsi:])
    price_low = list(data['low'][n_rsi:])
    price_close = list(data['close'][n_rsi:])
    rsi_list = list(get_data_from_db.get_rsi(company)['rsi'])
    for i in range(0, len(price_close)):
        if i < n_dd * 2: continue
        if STATUS != '':
            check_position(price_close[i - n_dd:i], company, date[i])
            continue
        calculate(price_low[i - n_dd * 2:i], price_high[i - n_dd * 2:i], n_dd, rsi_list[i - n_dd * 2:i], date[i],
                  company)


def calculate(price_low, price_high, n_dd, rsi_list, date, company):
    global STATUS
    price_low.reverse()
    price_high.reverse()
    rsi_list.reverse()

    first_price_buy = price_low[0]
    first_rsi_buy = rsi_list[0]
    first_price_sell = price_high[0]
    first_rsi_sell = rsi_list[0]

    Condition5 = True
    Condition6 = True

    for count in range(0, n_dd):
        if first_price_buy > price_low[count]: Condition5 = False
        if first_price_sell < price_high[count]: Condition6 = False

    second_price_buy = price_low[n_dd]
    second_rsi_buy = rsi_list[n_dd]

    second_price_sell = price_high[n_dd]
    second_rsi_sell = rsi_list[n_dd]

    for count in range(n_dd, n_dd * 2):
        if second_price_buy > price_low[count]:
            second_price_buy = price_low[count]
            second_rsi_buy = rsi_list[count]

        if second_price_sell < price_high[count]:
            second_price_sell = price_high[count]
            second_rsi_sell = rsi_list[count]

    Condition1 = first_price_buy < second_price_buy
    Condition2 = first_rsi_buy > second_rsi_buy

    Condition3 = first_price_sell > second_price_sell
    Condition4 = first_rsi_sell < second_rsi_sell

    if Condition1 and Condition2 and Condition5 and first_rsi_buy != 0 and second_rsi_buy != 0:
        STATUS = 'buy'
        set_status(STATUS, date, company)
    elif Condition3 and Condition4 and Condition6 and first_rsi_sell != 0 and second_rsi_sell != 0:
        STATUS = 'sell'
        set_status(STATUS, date, company)


def check_position(price, company, date):
    global FLAGGER_BUY
    global FLAGGER_SELL
    global STATUS
    price.reverse()
    checker = 0
    if STATUS != '':
        if STATUS == 'buy':
            if FLAGGER_BUY < 10:
                FLAGGER_BUY += 1
            else:
                FLAGGER_BUY = 0
                for counter in range(0, 5):
                    prev_ = price[0]
                    next_ = price[counter]
                    if prev_ < next_: checker = checker + 1;
                if checker >= 2:
                    STATUS = ''
                    set_status('closed', date, company)

        if STATUS == 'sell':
            if FLAGGER_SELL < 10:
                FLAGGER_SELL += 1
            else:
                FLAGGER_SELL = 0
                for counter in range(0, 5):
                    prev_ = price[0]
                    next_ = price[counter]
                    if next_ < prev_: checker = checker + 1;

                if checker >= 2:
                    STATUS = ''
                    set_status('closed', date, company)


def set_status(status, date, company):
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    cursor.execute(
        "update status set company_status='{}', date = '{}' where company='{}'".format(status, date, company))
    cursor.close()
