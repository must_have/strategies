import smtplib


def test():
    HOST = "mySMTP.server.com"
    SUBJECT = "Test email from Python"
    TO = "true.tkachyk@gmail.com"
    FROM = "denzhvan@gmail.com"
    text = "Python 3.4 rules them all!"

    BODY = "\r\n".join((
        "From: %s" % FROM,
        "To: %s" % TO,
        "Subject: %s" % SUBJECT,
        "",
        text
    ))

    server = smtplib.SMTP(HOST)
    server.sendmail(FROM, [TO], BODY)
    server.quit()
