import mysql.connector


# reserve server user='admin', password='zxasqw12', host='192.168.2.54

def mySqlConnect(name='historical_data'):
    try:
        if name is None:
            # if you want to create schema:
            DB = mysql.connector.connect(user='root', password='root',
                                         host='localhost')
        else:
            # if you have already created scheme:
            DB = mysql.connector.connect(user='root', password='root',
                                         host='localhost', database=name)
        return DB
    except mysql.connector.Error as e:
        print('Error: ' + str(e))
        return False
