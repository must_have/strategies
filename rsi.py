import get_data_from_db
import data_preparation
import pandas as pd
import numpy as np
from pandas_datareader import data as web
import matplotlib.pyplot as plt
import getDB


def read_all_lines(companies, n):
    DB = getDB.mySqlConnect()
    temporary_cursor = DB.cursor()
    temporary_cursor.execute('truncate rsi')
    for company in companies:
        print(company, " -calculate rsi")
        df = get_data_from_db.get_historical_data_from_table('data', company)
        rsi(df, n, company)


def rsi(data, n, company):
    date = data['date']
    price = data['close']
    UpAvg = 0
    DownAvg = 0
    RSI = 0
    RSI_list = []
    new_date = []
    for current_bar in range(0, len(price) - 1):
        if current_bar < n: continue

        if current_bar == n and n > 0:
            UpSum = 0
            DownSum = 0
            upAmt = 0
            downAmt = 0

            for counter in range(current_bar - n, current_bar):
                upAmt = float(price[counter + 1]) - float(price[counter])
                if upAmt >= 0:
                    downAmt = 0
                else:
                    downAmt = -upAmt
                    upAmt = 0
                UpSum += upAmt
                DownSum += downAmt
            UpAvg = UpSum / n
            DownAvg = DownSum / n
        elif current_bar > n > 0:
            upAmt = float(price[current_bar + 1]) - float(price[current_bar])
            if upAmt >= 0:
                downAmt = 0
            else:
                downAmt = -upAmt
                upAmt = 0
            UpAvg = (UpAvg * (n - 1) + upAmt) / n
            DownAvg = (DownAvg * (n - 1) + downAmt) / n
        if UpAvg + DownAvg != 0:
            RSI = 100 * UpAvg / (UpAvg + DownAvg)
        else:
            RSI = 0
        RSI_list.append(RSI)
        new_date.append(str(date[current_bar]))

    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    for i in range(0, len(RSI_list)):
        cursor.execute("insert into rsi(`company`,`value`,`date`) values('%s','%s','%s')" % (
            company, str(RSI_list[i]), str(new_date[i])))
    cursor.close()
