import getDB
import pandas as pd


def get_historical_data_from_table(name_of_table, company):
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    company_dict = {}
    price_high = []
    price_low = []
    price_close = []
    date = []
    print('select data from db')
    df = pd.read_sql("select * from {} where company = '{}'".format(name_of_table, company), DB)

    return df


def get_companies():
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    companies = []
    cursor.execute('select distinct company from data')
    print('select companies')
    for data in cursor:
        companies.append(data[0])
    cursor.close()
    return companies


def get_rsi(company):
    DB = getDB.mySqlConnect()
    cursor = DB.cursor()
    df = pd.read_sql('select value from rsi where company="{}"'.format(company), DB)
    return df
